package com.surfacetension.e_commers;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.surfacetension.e_commers.adapters.ChildCategoryAdapter;
import com.surfacetension.e_commers.adapters.RecyclerTouchListener;
import com.surfacetension.e_commers.api.ApiClient;
import com.surfacetension.e_commers.api.ApiInterface;
import com.surfacetension.e_commers.model.Category;
import com.surfacetension.e_commers.model.Product_;
import com.surfacetension.e_commers.model.ProductsFromAPI;
import com.surfacetension.e_commers.model.Ranking;
import com.surfacetension.e_commers.utility.Constatnts;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    /**
            -  "persist the data​ ​into​ ​a​ ​local​ ​datastore"  am I supposes to use sqlite DB?  Anyways I could have use sqlite DB, Realm DB or shered preferences.
            -  As time constraints am not implemented material design, icons, colors.
            -  Thanks for this assignment I really enjoyed it.
    **/


    public List<Category> selectedCategories = new ArrayList<>();
    private List<Category> finalList = new ArrayList<>();
    private List<Category> productList = new ArrayList<>();

    List<Product_> mViewed = new ArrayList<>();
    List<Product_> mOrdeRed = new ArrayList<>();
    List<Product_> mShaRed = new ArrayList<>();

    private RecyclerView recyclerView;


    private ChildCategoryAdapter mAdapter;
    int selectedCatLevel = 0;
    HashMap<Integer, List<Category>> catLevelMap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.category_rv);

        getDataFromAPI();

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                // AS PER CATEGORY LEVEL ADD DATA TO THE ADAPTER - ONLY ONE ADAPTER USER FOR CATEGORY LEVELS

                Category category = selectedCategories.get(position);

                //IF CATEGORY LEVEL REACHES END ONE NEW ACTIVITY (ChildCategoryActivity)
                if (selectedCatLevel == 0) {

                    Intent intent = new Intent(MainActivity.this, ChildCategoryActivity.class);
                    intent.putExtra(Constatnts.CHILD_CATEGORIES, (Serializable) category);

                    intent.putExtra(Constatnts.MOST_VIEWED, (Serializable) mViewed);
                    intent.putExtra(Constatnts.MOST_ORDERED, (Serializable) mOrdeRed);
                    intent.putExtra(Constatnts.MOST_SHARED, (Serializable) mShaRed);

                    startActivity(intent);
                }

                if (selectedCatLevel > 1) {
                    List<Category> catList = new ArrayList<>();

                    for (int i = 0; i < category.getChildCategories().size(); i++) {

                        int childCategories = category.getChildCategories().get(i);

                        for (int k = 0; k < catLevelMap.get(selectedCatLevel - 1).size(); k++) {

                            Category subCategory = catLevelMap.get(selectedCatLevel - 1).get(k);
                            int categoryId = subCategory.getId();

                            if (childCategories == categoryId) {
                                catList.add(subCategory);
                                break;
                            }
                        }
                    }

                    if (catList.size() > 0) {

                        selectedCatLevel = selectedCatLevel - 1;
                        selectedCategories.clear();
                        selectedCategories = catList;//catLevelMap.get(selectedCatLevel);
                        prepareData(catList);

                    }
                } else {

                    List<Category> catList = new ArrayList<>();

                    for (int i = 0; i < category.getChildCategories().size(); i++) {

                        int childCategories = category.getChildCategories().get(i);

                        for (int k = 0; k < productList.size(); k++) {

                            Category subCategory = productList.get(k);
                            int categoryId = subCategory.getId();

                            if (childCategories == categoryId) {
                                catList.add(subCategory);
                                break;
                            }
                        }
                    }

                    if (catList.size() > 0) {

                        selectedCatLevel = selectedCatLevel - 1;
                        selectedCategories.clear();
                        selectedCategories = catList;
                        prepareData(catList);

                    }
                }
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

    }

    @Override
    public void onBackPressed() {
        // HANDLE BACK KEY PRESSED - MANAGE CATEGORY LEVEL HERE
        if(selectedCatLevel != 2 ){
            getDataFromAPI();
        }else{
            super.onBackPressed();
        }
    }

    private void getDataFromAPI(){
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ProductsFromAPI> call = apiService.getAllProducts();
        call.enqueue(new Callback<ProductsFromAPI>() {
            @Override
            public void onResponse(Call<ProductsFromAPI>call, Response<ProductsFromAPI> response) {
                // GET RANKING DATA
                List<Ranking> rankings = response.body().getRankings();
                for (int j = 0; j < rankings.size(); j++) {
                    Ranking ranking = rankings.get(j);
                    switch (ranking.getRanking()){
                        case Constatnts.MOST_VIEWED:
                            mViewed = ranking.getProducts();
                            break;
                        case Constatnts.MOST_ORDERED:
                            mOrdeRed = ranking.getProducts();
                            break;
                        case Constatnts.MOST_SHARED:
                            mShaRed = ranking.getProducts();
                            break;
                    }
                }

                // CREATED HASH MAP FOR PERSIST THE DATA - AS PER CATEGORIES LEVEL
                catLevelMap = new HashMap<>();
                List<Category> responseCategories = new ArrayList<>();
                List<Category> responseData = response.body().getCategories();

                for (int i = 0; i < responseData.size(); i++) {
                    Category category = responseData.get(i);

                    if(category.getChildCategories().size() == 0) {
                        productList.add(category);
                    }else{
                        responseCategories.add(category);
                    }
                }

                catLevelMap.put(1, responseCategories);

                int level = 1;
                do {
                    finalList = new ArrayList<>();
                    for (int j = 0; j < catLevelMap.get(level).size(); j++) {
                        Category category = catLevelMap.get(level).get(j);
                        for (int i = 0; i < category.getChildCategories().size() ; i++) {
                            int childCategories =  category.getChildCategories().get(i);
                            for (int k = 0; k <catLevelMap.get(level).size() ; k++) {
                                int categoryId = catLevelMap.get(level).get(k).getId();
                                if(childCategories == categoryId){
                                    finalList.add(category);
                                    break;
                                }
                            }
                            break;
                        }
                    }

                    if(finalList.size() > 0) {

                        List<Category> union = new ArrayList<Category>(catLevelMap.get(level));
                        union.addAll(finalList);
                        List<Category> intersection = new ArrayList<Category>(catLevelMap.get(level));
                        intersection.retainAll(finalList);
                        union.removeAll(intersection);

                        catLevelMap.put(level, union);
                        catLevelMap.put(level+1, finalList);
                    }

                    level++;

                } while (finalList.size() != 0);

                if (catLevelMap.size()>0){
                    selectedCategories.clear();
                    selectedCatLevel = catLevelMap.size();
                    selectedCategories = catLevelMap.get(selectedCatLevel);
                    prepareData(selectedCategories);
                }
            }

            @Override
            public void onFailure(Call<ProductsFromAPI>call, Throwable t) {
                }
        });
    }

    private void prepareData(List<Category> categories) {

        mAdapter = new ChildCategoryAdapter(categories);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
    }



}
