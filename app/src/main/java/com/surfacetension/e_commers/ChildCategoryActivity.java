package com.surfacetension.e_commers;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.surfacetension.e_commers.adapters.ChildCategoryAdapter;
import com.surfacetension.e_commers.adapters.ProductsAdapter;
import com.surfacetension.e_commers.adapters.RecyclerTouchListener;
import com.surfacetension.e_commers.model.Category;
import com.surfacetension.e_commers.model.Product;
import com.surfacetension.e_commers.model.Product_;
import com.surfacetension.e_commers.utility.Constatnts;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ChildCategoryActivity extends AppCompatActivity {
    List<Product> products;
    private RecyclerView recyclerView;

    private LinearLayout bottombarLilay;
    private TextView viewedTv;
    private TextView orderedTv;
    private TextView sharedTv;

    private Category mCategory = new Category();
    private ProductsAdapter mAdapter;

    List<Product_> mViewed = new ArrayList<>();
    List<Product_> mOrdeRed = new ArrayList<>();
    List<Product_> mShaRed = new ArrayList<>();

    final static String ASC = "asc";
    final static String DESC = "desc";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child_category);

        mCategory = (Category) getIntent().getSerializableExtra(Constatnts.CHILD_CATEGORIES);

        mViewed = (List<Product_>) getIntent().getSerializableExtra(Constatnts.MOST_VIEWED);
        mOrdeRed = (List<Product_>) getIntent().getSerializableExtra(Constatnts.MOST_ORDERED);
        mShaRed = (List<Product_>) getIntent().getSerializableExtra(Constatnts.MOST_SHARED);

        recyclerView = (RecyclerView) findViewById(R.id.category_rv);
        bottombarLilay = (LinearLayout) findViewById(R.id.bottombar_lilay);

        viewedTv = (TextView) bottombarLilay.findViewById(R.id.viewed_tv);
        orderedTv = (TextView) bottombarLilay.findViewById(R.id.ordered_tv);
        sharedTv = (TextView) bottombarLilay.findViewById(R.id.shared_tv);

        setClickLster();

        products = mCategory.getProducts();
        mAdapter = new ProductsAdapter(products);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                Product product = products.get(position);

                Intent intent = new Intent(ChildCategoryActivity.this, ProductDetailActivity.class);
                intent.putExtra(Constatnts.PRODUCT_DETAILS, (Serializable) product);
                startActivity(intent);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void setClickLster(){

        viewedTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowDialog(Constatnts.MOST_VIEWED);
            }
        });

        orderedTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowDialog(Constatnts.MOST_ORDERED);
            }
        });

        sharedTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowDialog(Constatnts.MOST_SHARED);
            }
        });
    }

    private void ShowDialog(final String title){

        AlertDialog.Builder builder = new AlertDialog.Builder(ChildCategoryActivity.this);
        builder.setTitle(title);
        builder.setPositiveButton(ASC, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                SharedPreferences.Editor editor = getSharedPreferences("MY_PREFS_NAME", MODE_PRIVATE).edit();
                editor.putString(Constatnts.SORT_ORDER, ASC);
                editor.apply();

                switch (title){
                    case Constatnts.MOST_VIEWED:
                        Collections.sort(mViewed, new ViewComparator());
                        sortProducts(mViewed);
                        break;
                    case Constatnts.MOST_ORDERED:
                        Collections.sort(mOrdeRed, new OrderComparator());
                        sortProducts(mOrdeRed);
                        break;
                    case Constatnts.MOST_SHARED:
                        Collections.sort(mShaRed, new ShareComparator());
                        sortProducts(mShaRed);
                        break;
                }
            }
        });

        builder.setNegativeButton(DESC, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                SharedPreferences.Editor editor = getSharedPreferences("MY_PREFS_NAME", MODE_PRIVATE).edit();
                editor.putString(Constatnts.SORT_ORDER, DESC);
                editor.apply();

                switch (title){
                    case Constatnts.MOST_VIEWED:
                        Collections.sort(mViewed, new ViewComparator());
                        sortProducts(mViewed);
                        break;
                    case Constatnts.MOST_ORDERED:
                        Collections.sort(mOrdeRed, new OrderComparator());
                        sortProducts(mOrdeRed);
                        break;
                    case Constatnts.MOST_SHARED:
                        Collections.sort(mShaRed, new ShareComparator());
                        sortProducts(mShaRed);
                        break;
                }
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.show();
    }

    public class ViewComparator implements Comparator<Product_> {
        public int compare(Product_ left, Product_ right) {

            SharedPreferences prefs = getSharedPreferences("MY_PREFS_NAME", MODE_PRIVATE);
            String sortedText = prefs.getString(Constatnts.SORT_ORDER, ASC);

            if(sortedText.equalsIgnoreCase(ASC)){
                return left.getViewCount().compareTo(right.getViewCount());
            }else{
                return right.getViewCount().compareTo(left.getViewCount());
            }
        }
    }

    public class OrderComparator implements Comparator<Product_>
    {
        public int compare(Product_ left, Product_ right) {

            SharedPreferences prefs = getSharedPreferences("MY_PREFS_NAME", MODE_PRIVATE);
            String sortedText = prefs.getString(Constatnts.SORT_ORDER, ASC);

            if(sortedText.equalsIgnoreCase(ASC)){
                return left.getOrderCount().compareTo(right.getOrderCount());
            }else{
                return right.getOrderCount().compareTo(left.getOrderCount());
            }

        }
    }

    public class ShareComparator implements Comparator<Product_>
    {
        public int compare(Product_ left, Product_ right) {
            SharedPreferences prefs = getSharedPreferences("MY_PREFS_NAME", MODE_PRIVATE);
            String sortedText = prefs.getString(Constatnts.SORT_ORDER, ASC);

            if(sortedText.equalsIgnoreCase(ASC)) {
                return left.getShares().compareTo(right.getShares());
            }else{
                return right.getShares().compareTo(left.getShares());
            }
        }
    }

    private void sortProducts(List<Product_> sortedIds) {
        List<Product> productList = new ArrayList<>();

        for (int i = 0; i < sortedIds.size(); i++) {
            int id = sortedIds.get(i).getId();
            for (int k = 0; k < products.size(); k++) {
                Product product = products.get(k);
                int categoryId = product.getId();
                if (id == categoryId) {
                    productList.add(product);
                    break;
                }
            }
        }
        prepareData(productList);
    }

    private void prepareData(List<Product> finalproducts) {
        mAdapter = new ProductsAdapter(finalproducts);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
    }
}
