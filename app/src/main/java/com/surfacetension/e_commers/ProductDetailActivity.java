package com.surfacetension.e_commers;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.surfacetension.e_commers.adapters.DetailsAdapter;
import com.surfacetension.e_commers.adapters.RecyclerTouchListener;
import com.surfacetension.e_commers.model.Product;
import com.surfacetension.e_commers.model.Variant;
import com.surfacetension.e_commers.utility.Constatnts;

import java.util.ArrayList;
import java.util.List;

public class ProductDetailActivity extends AppCompatActivity {

    Product mProduct;
    List<Variant> mVariants = new ArrayList<>();
    private RecyclerView recyclerView;
    private DetailsAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        mProduct = (Product) getIntent().getSerializableExtra(Constatnts.PRODUCT_DETAILS);
        mVariants = mProduct.getVariants();

        recyclerView = (RecyclerView) findViewById(R.id.details_rv);

        mAdapter = new DetailsAdapter(mVariants);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }
}
