package com.surfacetension.e_commers.utility;

public class Constatnts {
    private static final Constatnts ourInstance = new Constatnts();

    public static Constatnts getInstance() {
        return ourInstance;
    }

    private Constatnts() {
    }

    public static final String CHILD_CATEGORIES = "child_categories";
    public static final String PRODUCT_DETAILS = "product_details";
    public static final String MOST_VIEWED = "Most Viewed Products";
    public static final String MOST_ORDERED = "Most OrdeRed Products";
    public static final String MOST_SHARED = "Most ShaRed Products";
    public static final String SORT_ORDER = "sort_order";
    public static final String APP_NAME = "ecommers";


}
