package com.surfacetension.e_commers.api;

import com.surfacetension.e_commers.model.ProductsFromAPI;
import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {

    @GET("/json")
    Call<ProductsFromAPI> getAllProducts();
}
