package com.surfacetension.e_commers.adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.surfacetension.e_commers.R;
import com.surfacetension.e_commers.model.Variant;

import java.util.List;

/**
 * Created by dipak patil on 07/11/18.
 */
public class DetailsAdapter extends RecyclerView.Adapter<DetailsAdapter.MyViewHolder> {

    private List<Variant> variantList;

    public DetailsAdapter(List<Variant> variants) {
        this.variantList = variants;
    }

    @Override
    public DetailsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.variant_list_item, parent, false);

        return new DetailsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DetailsAdapter.MyViewHolder holder, int position) {
        Variant variant = variantList.get(position);
        holder.colorName.setText(variant.getColor());
        int  price = variant.getPrice();
        holder.priceTv.setText("Price: " + price);

        Object size = variant.getSize();
        if(size != null ) {
            holder.size.setText("Size: "+ size);
        }

    }

    @Override
    public int getItemCount() {
        return variantList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView colorName;
        public TextView priceTv;
        public TextView tax;
        public TextView size;

        public MyViewHolder(View view) {
            super(view);
            colorName = (TextView) view.findViewById(R.id.color_tv);
            priceTv = (TextView) view.findViewById(R.id.price_tv);
//            tax = (TextView) view.findViewById(R.id.tax_tv);
            size = (TextView) view.findViewById(R.id.size_tv);

        }
    }
}