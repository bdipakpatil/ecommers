package com.surfacetension.e_commers.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.surfacetension.e_commers.R;
import com.surfacetension.e_commers.model.Product;

import java.util.List;

/**
 * Created by dipak patil on 07/11/18.
 */
public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.MyViewHolder> {

    private List<Product> productList;

    public ProductsAdapter(List<Product> catList) {
        this.productList = catList;
    }

    @Override
    public ProductsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_list_item, parent, false);

        return new ProductsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ProductsAdapter.MyViewHolder holder, int position) {
        Product product = productList.get(position);
        holder.title.setText(product.getName());
        holder.tax.setText("TAX - "+product.getTax().getName()+" "+product.getTax().getValue());

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public TextView tax;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.product_name_tv);
            tax = (TextView) view.findViewById(R.id.tax_tv);

        }
    }
}